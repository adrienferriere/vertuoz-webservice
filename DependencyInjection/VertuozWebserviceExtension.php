<?php

namespace Vertuoz\WebserviceBundle\DependencyInjection;

use Vertuoz\WebserviceBundle\DependencyInjection\Configuration;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class VertuozWebserviceExtension extends Extension implements PrependExtensionInterface {
    
    public function prepend(ContainerBuilder $container)
    {
        
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
    
    
    public function load(array $configs, ContainerBuilder $container) {
        
        $configuration = new Configuration();
     
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('vertuoz_webservice.cdn.url', $config['cdn']['url']);
        $container->setParameter('vertuoz_webservice.api.base_url', $config['api']['base_url']);
        $container->setParameter('vertuoz_webservice.api.path', $config['api']['path']);
        $container->setParameter('vertuoz_webservice.api.options.timeout', $config['api']['options']['timeout']);
        $container->setParameter('vertuoz_webservice.api.options.http_errors', $config['api']['options']['http_errors']);
        $container->setParameter('vertuoz_webservice.api.options.headers.content_type', $config['api']['options']['headers']['content_type']);
        $container->setParameter('vertuoz_webservice.api.options.headers.user_agent', $config['api']['options']['headers']['user_agent']);
        $container->setParameter('vertuoz_webservice.api.options.headers.accept', $config['api']['options']['headers']['accept']);
        $container->setParameter('vertuoz_webservice.api.options.headers.token', $config['api']['options']['headers']['token']);
        
        
    }


}
