<?php

namespace Vertuoz\WebserviceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('vertuoz_webservice');
        $rootNode
            ->children()
            ->arrayNode('cdn')
                ->children()
                    ->scalarNode('url')->end()
                ->end()
            ->end() // cdn
            ->arrayNode('api')
                ->children()
                    ->scalarNode('base_url')->end()
                ->end()
                ->children()
                    ->scalarNode('path')->end()
                ->end()
                ->children()
                    ->arrayNode('options')
                        ->children()
                            ->integerNode('timeout')->end()
                        ->end()
                        ->children()
                            ->scalarNode('http_errors')->end()
                        ->end()
                        ->children()
                            ->arrayNode('headers')
                                ->children()
                                    ->scalarNode('user_agent')->end()
                                ->end()
                                ->children()
                                    ->scalarNode('content_type')->end()
                                ->end()
                                ->children()
                                    ->scalarNode('accept')->end()
                                ->end()
                                ->children()
                                    ->scalarNode('token')->end()
                                ->end() 
                            ->end() // headers
                        ->end()
                    ->end() // option
                ->end()
            ->end() // api
        ;

        return $treeBuilder;
    }
}