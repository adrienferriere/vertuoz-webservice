<?php

namespace Vertuoz\WebserviceBundle\Api;

use GuzzleHttp\Client;

class ApiClient {

    protected $apiVersion;
    protected $path;
    protected $guzzleClient;
    protected $params;

    /**
     * 
     * @param \GuzzleHttp\Client $guzzleClient
     * @param type $apiVersion
     */
    public function __construct(Client $guzzleClient, $apiPath = '', $params) {
        
        $this->guzzleClient = $guzzleClient;
        $this->path = $apiPath;
        $this->params = $params;
        
    }

    /**
     * 
     * @param string $url
     * @return type
     */
    public function get($url) {
        
        $response = $this->guzzleClient->get($this->path.$url, $this->params)->getBody()->getContents();
        
        return json_decode($response, true);
    }

    /**
     * 
     * @param string $url
     * @return type
     */
    public function post($url, $data) {
        
        $params = array_merge($this->params, array('form_params' => $data));
        
        $response = $this->guzzleClient->post($this->path.$url, $params)->getBody()->getContents();

        return json_decode($response, true);
    }

}
