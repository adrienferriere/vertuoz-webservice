<?php

namespace Vertuoz\WebserviceBundle\Api\Render;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use GuzzleHttp\Exception\ClientException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Helper\SliderHelper;

class SliderController extends Controller
{
    /**
     * @Route("/render-slider", name="render_slider", methods={"GET"})
     */
    public function renderSlider(Request $request)
    {
        $apiClient = $this->get('api.client');
        $twigData = array();
        $debug = $this->getParameter('kernel.debug');

        $tplName = $request->query->get('tplName', 'default');
        $name = $request->query->get('name');
        
        if (!$name) {
            throw new BadRequestHttpException("Name is missing");
        }
        
        $params['name'] = $name;

        $baseViewPath = "api/slider";
        $viewPath = $baseViewPath . "/" . $tplName;
   
        try {
            $sliderHelper = new SliderHelper($apiClient);
            $slider = $sliderHelper->get($params);
        } catch (ClientException $e) { 
            if ($debug) {
                $viewPath = "api/error/notfound.html.twig";
                return $this->render($viewPath, array("message" => sprintf("Erreur API : %s", $this->getParameter('api.base_uri') . $e->getRequest()->getRequestTarget())));
            }
        }
        
        if (empty($slider) && $debug) {
            $viewPath = "api/error/notfound.html.twig";
            return $this->render($viewPath, array("message" => sprintf("Slider <strong>%s</strong> non trouvé", $params['name'])));
        }
        
        $twigData["slider"] = $slider;
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
}
