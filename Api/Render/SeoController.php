<?php

namespace Vertuoz\WebserviceBundle\Api\Render;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use GuzzleHttp\Exception\ClientException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Helper\SeoHelper;

class SeoController extends Controller
{
    /**
     * @Route("/render-seo", name="render_seo", methods={"GET"})
     */
    public function renderSeo(Request $request)
    {
        $apiClient = $this->get('api.client');
        $twigData = array();
        $debug = $this->getParameter('kernel.debug');
        $tplName = $request->query->get('tplName', 'default');
  
        $params = $request->query->get('params');
        $seo = [];
        
        $baseViewPath = "@VertuozWebservice/Api";
        $viewPath = $baseViewPath . "/seo/" . $tplName;
        
        if (isset($params['uri'])) {
        
            $baseParams = [
                'uri' => '',
                'locale' => $request->getLocale()
            ];

            $fullParams = array_merge($baseParams, [
                'uri' => $params['uri'],
                'locale' => isset($params['locale']) ? $params['locale'] : null
            ]);

            try {
                $seoHelper = new SeoHelper($apiClient);
                $seo = $seoHelper->get($params);

            } catch (ClientException $e) { 
                if ($debug) {
                    $viewPath = $baseViewPath . "/error/notfound.html.twig";
                    return $this->render($viewPath, array("message" => sprintf("Erreur API : %s", $this->getParameter('api.base_uri') . $e->getRequest()->getRequestTarget())));
                }
            }

            if (empty($seo) && $debug) {
                $viewPath = $baseViewPath . "/error/notfound.html.twig";
                return $this->render($viewPath, array("message" => sprintf("SEO uri <strong>%s</strong> non trouvé", $params['uri'])));
            }
            
        }
        else {
            
            $seo['robots'] = 'index,follow';
            
            if (isset($params['title'])) {
                $seo['title'] = $params['title'];
            }
            
            if (isset($params['description'])) {
                $seo['description'] = $params['description'];
            }
            
            if (isset($params['robots'])) {
                $seo['robots'] = $params['robots'];
            }
            
        }
 
        $twigData["seo"] = $seo;
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
}
