<?php

namespace Vertuoz\WebserviceBundle\Api\Render;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Vertuoz\WebserviceBundle\Api\Helper\ContentHelper;
use GuzzleHttp\Exception\ClientException;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\DefaultView;

class ContentController extends Controller
{
    
    /**
     * @Route("/render-content-collection", name="render_content_collection", methods={"GET"})
     */
    public function renderContentCollection(Request $request)
    {
        
        $apiClient = $this->get('api.client');
        $debug = $this->getParameter('kernel.debug');
        $locale = $request->getLocale();
        $twigData = [];

        $collectionTplName = $request->query->get('collectionTplName', 'default');
        $page = $request->query->get('pager_page', 1);
        $perPage = $request->query->get('perPage', 999);
        $areaName = $request->query->get('areaName');
        $tplName = $request->query->get('tplName', 'default');
        $noElementText = $request->query->get('noElementText', 'Aucun élément à afficher');
        $params = $request->query->get('params', []);
        
        if (!$areaName) {
            throw new BadRequestHttpException("Area is missing");
        }
         
       $params['areaName'] = $areaName;
     
        $baseViewPath = "@VertuozWebservice/Api";
        $viewPath = $baseViewPath . "/content-collection/" . $collectionTplName;
 
        try {
            $contentHelper = new ContentHelper($apiClient, $locale);
            $contents = $contentHelper->listContents($params);
            
        } catch (ClientException $e) {
            if ($debug) {
                $viewPath = $baseViewPath . "/error/notfound.html.twig";
                return $this->render($viewPath, array("message" => sprintf("Erreur API : %s", $this->getParameter('api.base_uri') . $e->getRequest()->getRequestTarget())));
            }
        }
    
        if (empty($contents) && $debug) {
            $viewPath = $baseViewPath . "/error/notfound.html.twig";
            return $this->render($viewPath, array("message" => sprintf("Contenus zone <strong>%s</strong> non trouvé", $params['areaName'])));
        }
        
        $adapter = new ArrayAdapter($contents);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta
        ->setMaxPerPage($perPage)
        ->setCurrentPage($page);
        
        $routeGenerator = function($page) {
            $request = Request::createFromGlobals();
            return $request->getPathInfo() . '?page='.$page;
        };
        
        $view = new DefaultView();
        $options = array('proximity' => 3);
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, $options);
        
        $twigData["contents"] = $pagerfanta->getCurrentPageResults();
        $twigData["pager"] = $pagerHtml;
        $twigData['areaName'] = $areaName;
        $twigData["params"] = $params;
        $twigData["tplName"] = $tplName;
        $twigData["noElementText"] = urldecode($noElementText);
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
    
    
    /**
     * @Route("/render-content", name="render_content", methods={"GET"})
     */
    public function renderContent(Request $request)
    {
        
        $apiClient = $this->get('api.client');
        $debug = $this->getParameter('kernel.debug');
        $locale = $request->getLocale();
        $twigData = [];
        $content = [];

        $id = $request->query->get('id');
        $code = $request->query->get('code');
        $tplName = $request->query->get('tplName', 'default');
        $titleTagName = $request->query->get('titleTagName', 'div');

        $baseViewPath = "@VertuozWebservice/Api";
        $viewPath = $baseViewPath . "/content/" . $tplName;

        $contentHelper = new ContentHelper($apiClient, $locale);

        if ($code) {

            try {
                $content = $contentHelper->getOneByCode($code);
            } catch (ClientException $e) {
                if ($debug) {
                    $viewPath = $baseViewPath . "/error/notfound.html.twig";
                    return $this->render($viewPath, array("message" => sprintf("Erreur API : %s", $this->getParameter('api.base_uri') . $e->getRequest()->getRequestTarget())));
                }
            }
            
            if (empty($content) && $debug) {
                $viewPath = $baseViewPath . "/error/notfound.html.twig";
                return $this->render($viewPath, array("message" => sprintf("Contenu code <strong>%s</strong> non trouvé", $code)));
            }

        }
        elseif ($id) {

            try {
                $content = $contentHelper->getOneById($id);
            } catch (ClientException $e) {
                if ($debug) {
                    $viewPath = $baseViewPath . "/error/notfound.html.twig";
                    return $this->render($viewPath, array("message" => sprintf("Erreur API : %s", $this->getParameter('api.base_uri') . $e->getRequest()->getRequestTarget())));
                }
            }
            
            if (empty($content) && $debug) {
                $viewPath = $baseViewPath . "/error/notfound.html.twig";
                return $this->render($viewPath, array("message" => sprintf("Contenu id <strong>%s</strong> non trouvé", $id)));
            }

        }
        elseif ($slug) {

            try {
                $content = $contentHelper->getOneBySlug($slug);
            } catch (ClientException $e) {
                if ($debug) {
                    $viewPath = $baseViewPath . "/error/notfound.html.twig";
                    return $this->render($viewPath, array("message" => sprintf("Erreur API : %s", $this->getParameter('api.base_uri') . $e->getRequest()->getRequestTarget())));
                }
            }
            
            if (empty($content) && $debug) {
                $viewPath = $baseViewPath . "/error/notfound.html.twig";
                return $this->render($viewPath, array("message" => sprintf("Contenu slug <strong>%s</strong> non trouvé", $slug)));
            }

        }
        
        
        $twigData["content"] = $content;
        $twigData["titleTagName"] = $titleTagName;
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
    
}
