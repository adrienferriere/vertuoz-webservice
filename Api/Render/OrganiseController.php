<?php

namespace Vertuoz\WebserviceBundle\Api\Render;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Helper\OrganiseHelper;

class OrganiseController extends Controller
{
    /**
     * @Route("/render-organise", name="render_organise", methods={"GET"})
     */
    public function renderOrganise(Request $request)
    {
        $apiClient = $this->get('api.client');
        $twigData = array();
        $debug = $this->getParameter('kernel.debug');

        $tplName = $request->query->get('tplName', 'default');
        $params = $request->query->get('params');
        
        if (!$params['area']) {
            throw new BadRequestHttpException("Area is missing");
        }
     
        $baseViewPath = "@VertuozWebservice/Api";
        $viewPath = $baseViewPath . "/organise/" . $collectionTplName;
  
        try {
            $organiseHelper = new OrganiseHelper($apiClient);
            $organise = $organiseHelper->get($params);
            
        } catch (ClientException $e) {
            if ($debug) {
                $viewPath = $baseViewPath . "/error/notfound.html.twig";
                return $this->render($viewPath, array("message" => sprintf("Erreur API : %s", $this->getParameter('api.base_uri') . $e->getRequest()->getRequestTarget())));
            }
        }

        if (empty($organise) && $debug) {
            $viewPath = $baseViewPath . "/error/notfound.html.twig";
            return $this->render($viewPath, array("message" => sprintf("Organise zone <strong>%s</strong> non trouvé", $params['area'])));
        }
        
        $twigData["organise"] = $organise;
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
}
