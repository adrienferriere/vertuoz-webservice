<?php

namespace Vertuoz\WebserviceBundle\Api\Helper;

class SliderHelper {

    protected $client;

    public function __construct(\App\Vertuoz\Api\ApiClient $client) {
        $this->client = $client;
    }

    public function get(array $params) {
        return $this->client->get('/slider?' . http_build_query($params));
    }

}
