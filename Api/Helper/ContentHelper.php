<?php

namespace Vertuoz\WebserviceBundle\Api\Helper;

class ContentHelper {

    protected $client;
    protected $locale;

    public function __construct(\Vertuoz\WebserviceBundle\Api\ApiClient $client, $locale) {
        $this->client = $client;
        $this->locale = $locale;
    }

    public static function slugify($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function listContents($params) {
        $params['language'] = $this->locale;
        return $this->client->get('/contents?' . http_build_query($params));
    }

    public function get($id) {
        
        $content = $this->client->get('/contents?id=' . $id . '&language=' . $this->locale);
        
        if (!empty($content)) {
            return $content[0];
        }
        return null;
        
    }

    public function getOneByCode(string $code) {
        
        $contents = $this->client->get('/contents?code=' . $code . '&language=' . $this->locale);

        if (!empty($contents)) {
            return $contents[0];
        }
        return null;
    }
    
    public function getOneBySlug(string $slug) {

        $contents = $this->client->get('/contents?readMoreLink=/' . $slug);
        if (!is_null($contents)) {
            return $contents[0];
        }
        return null;
    }

    public function getOneBySlugAndAreaNameNotIn(string $url, array $excludes)
    {
        $contents = $this->client->get('/contents?readMoreLink=/' . $url);
        foreach ($contents as $content) {
            if (!in_array($content['areaName'], $excludes)) {
                return $content;
            }
        }
        return null;
    }

    public function getAllByArea(string $areaName) {
        $contents = $this->client->get('/contents?areaName=' . $areaName);

        return $contents;
    }

}
