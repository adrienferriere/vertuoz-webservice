<?php

namespace Vertuoz\WebserviceBundle\Api\Helper;

class SeoHelper {

    protected $client;

    public function __construct(\App\Vertuoz\Api\ApiClient $client) {
        $this->client = $client;
    }

    public function get(array $params) {
        return $this->client->get('/seo/page?' . http_build_query($params));
    }

}
