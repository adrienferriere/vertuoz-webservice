<?php

namespace Vertuoz\WebserviceBundle\Api\Helper;

class OrganiseHelper {

    protected $client;

    public function __construct(\App\Vertuoz\Api\ApiClient $client) {
        $this->client = $client;
    }

    public function get(array $params) {
        return $this->client->get('/organise?' . http_build_query($params));
    }

}
