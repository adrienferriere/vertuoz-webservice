<?php

namespace Vertuoz\WebserviceBundle;

use Vertuoz\WebserviceBundle\DependencyInjection\VertuozWebserviceExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class VertuozWebserviceBundle extends Bundle
{
 public function getContainerExtension()
    {
        return new VertuozWebserviceExtension();
    }
}